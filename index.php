
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="ru"> <!--<![endif]-->

<head>

	<meta charset="utf-8">
	<title>ДЕМО ВЕРСИЯ КОМИКСОВ</title>
	<meta name="description" content="">
	<link rel="shortcut icon" href="img/icon.png" type="image/x-icon">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" href="libs/bootstrap/css/bootstrap-grid.min.css">
	<link rel="stylesheet" href="css/fonts.css">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/media.css">
</head>

<body>

<header>
	<div class="container">
		<div class="row">
			<div class="form_comics">
				<h1>Демо версия  <span>комиксов</span></h1>
				<p>После заполнения формы, на указанную электронную почту будет отправлена ссылка для скачивания демоверсии комикса <b>"Nikol & Alisa"</b>. Ссылки отправляются один раз в сутки. Если Вы не получили ссылку на скачивание, проверьте указанный адрес электронной почты или напишите письмо  <b>nikolalisashop@gmail.com</b> и мы пришлем ссылку на скачивание.</p>
				<form action="file.php" method="post">
		            <input type="text"  placeholder="Введите Ваше имя..." name="first_name" value="" required />
		            <input type="text"  placeholder="Введите Ваш E-mail..." name="email" value="" required />
		            <input type="text"  placeholder="Введите Ваш телефон..." name="custom_ref1" value="" required />
		            <input type="text"  placeholder="Укажите вашу страну и город..." name="country" value="" required />
		            <input type="text"  maxlength="2"  placeholder="Сколько вам лет?" name="oldman" value="" required />
		       		<button name="teata" class="btn">Получить бесплатную демоверсию</button>
				</form>
			</div>
		</div>
	</div>
</header>





	<!-- Здесь пишем код -->
	<div class="hidden"></div>
	<div class="loader"><div class="loader_inner"></div></div>
	<!--[if lt IE 9]>
	<script src="libs/html5shiv/es5-shim.min.js"></script>
	<script src="libs/html5shiv/html5shiv.min.js"></script>
	<script src="libs/html5shiv/html5shiv-printshiv.min.js"></script>
	<script src="libs/respond/respond.min.js"></script>
	<![endif]-->
	<script src="libs/jquery/jquery-1.11.2.min.js"></script>
	<script src="libs/waypoints/waypoints.min.js"></script>
	<script src="js/common.js"></script>
	
</body>
</html> 